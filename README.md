# DICE STM32

This repo contains Kicad files for DICE[1,2] PCBs with the smallest STM32 available (Jun 2024).

It can be programmed using Arduino by following the instructions here:

    https://github.com/FiberCircuits/firmware/tree/main/imu_fiber



## Requirements

Kicad:

    https://www.kicad.org/download/




## References

[1] The DICE project was documented by Zach Freddin here (2020):

    https://cba.mit.edu/docs/papers/20.09.DICE.pdf

[2] DICE verion by Alan Han (2024):

    https://gitlab.cba.mit.edu/Alan.Han/cba_bvb


## Overview

The prototype looks like this:

![prototype overview](https://gitlab.cba.mit.edu/honnet/dice-stm32/-/raw/main/laser_prototype/exports/stm32_laser-F_Cu.gbr.png)


